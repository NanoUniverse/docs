FeedTheBeast Academy
^^^^^^^^^^^^^^^^^^^^
Wersja - ``1.1.1``
------------------
* **Adres IP:** ``academy.nu-c.pl``

----


Opis Paczki
-------
Największym problemem gdy zaczynasz przygodę z zmodyfikowaną wersją gry Minecraft, jest to, że nie ma wyraźnego wyboru w który modpack grać. Nauka modów wymaga przeszukiwania wiki, tutoriali oraz oglądania filmów.
FTB Academy ma na celu rozwiązanie tego problemu. Jest zaprojektowany specjalnie dla osób, które nigdy nie grały z modami. Modpack ma ponad 300 zadań, które przeprowadzą Cię przez każdy etap rozwoju najpopularniejszych modów, oraz prawie 100 stron Przewodników, aby dać bardziej szczegółowe wyjaśnienie co i jak dzięki czemu nie musisz przeszukiwać wiki.

