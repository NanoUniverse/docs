Enigmatica 2 Expert
^^^^^^^^^^^^^^^^^^^^
Wersja - ``1.81a``
------------------
* **Adres IP:** ``e2e.nanouniverse.pl``

----


Opis Paczki
-------
* Celem paczki jest ukończenie linii zadań z kreatywnymi przedmiotami
* Ponad 650 zadań, które poprowadzą Cie przez grę.
* Mnóstwo niestandardowych przepisów
* Wiele niestandardowych światów

