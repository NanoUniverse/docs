++++++++
Komendy
++++++++

General
=======
* ``/spawn`` - Przenosi Cię na spawn
* ``/sethome`` - Ustawia lokalizację domu
* ``/pvp`` - Toggles PVP on/off
* ``/ignore <player>`` - Shows the ignore list or ignores a player
* ``/money`` - Pokazuje Twój stan konta
* ``/pay [player] [amount]`` - Wysyła pieniądze graczowi
* ``/vote`` - Vote for our server and get rewards
* ``/pomoc`` - Pokazuje dostępne komendy pomocy

Czat
====
* ``/msg [gracz] [wiadomość]`` - Sends a private wiadomość
* ``/r [wiadomość]`` - Reply to the last gracz that sent you a wiadomość
* ``/mail send [gracz] [wiadomość]`` -  Sends an offline wiadomość
* ``/mail read`` - Checks your offline wiadomość(s)
* ``/mail clear`` - Removes your offline wiadomość(s)

Claimy / Działki
======

See :ref:`ref-griefprevention` for more information

Market
======
* ``/market listings`` - Opens the buy interface
* ``/market mail`` - Opens your mailbox
* ``/market create [price] <amount>`` - Creates a listing for the market. You need to be holding the item you wish to list in your hand.
* ``/market pricecheck`` - Checks the recommended price of an item in your hand
* ``/market send [gracz] <amount>`` - Transfers the item (In your hand) to a gracz, this allows for long distance trading from gracz to gracz

