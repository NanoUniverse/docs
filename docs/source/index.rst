.. NanoUniverse documentation master file, created by
   sphinx-quickstart on Sat Jan 31 03:40:37 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

NanoUniverse Dokumentacja!
=================================================

Zawartość:

.. toctree::
   :maxdepth: 1

   NanoUniverse/index
   Serwery/index
   Baza_Wiedzy/index


Indices and tables
------------------

* :ref:`genindex`
* :ref:`search`

